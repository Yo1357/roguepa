﻿using UnityEngine;
using System.Collections;

public class Exit : MonoBehaviour {

	
    //1. Check for collision
    void OnTriggerEnter2D(Collider2D target)
    {
        //2. Check if playing is activating it
        if (target.gameObject.tag.Equals("Player") == true)
        {
            //3. Load New level
            Application.LoadLevel("Level 2");
        }

    }

    void Start () {
	
	}
	
	
	void Update () {
	
	}
}
