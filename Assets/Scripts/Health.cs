﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health : MonoBehaviour {
    [SerializeField]
    private Text health;
    private int playerHealth = 5;

	// Use this for initialization
	void Start () {

       
    }
	
	// Update is called once per frame
	void Update () {
        health.text = "Health: " + playerHealth;
    }
}
