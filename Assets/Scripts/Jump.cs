﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour {

    public int playerSpeed;
    public int jumpHeight;
    public bool isGrounded = false;

   


    void FixedUpdate()
    {
        isGrounded = Physics2D.Raycast(transform.position, -Vector3.up, 1f);
    }

    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey("right"))
        {
            transform.position -= Vector3.left * playerSpeed * Time.deltaTime;
        }

        if (Input.GetKey("left"))
        {
            transform.position -= Vector3.right * playerSpeed * Time.deltaTime;
        }

        if (Input.GetKeyDown("up") && isGrounded)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector3(0, jumpHeight, 0), ForceMode2D.Force);
        }

    }
}
