﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    private Rigidbody2D myRigidBody;
    private Animator myAnimator;
    [SerializeField]
    private float movementSpeed;
    private bool attack;
    private bool slide;
    private bool facingRight;
    [SerializeField]
    private Transform[] groundPoint;
    [SerializeField]
    private float groundRadius;
    [SerializeField]
    private LayerMask whatIsGround;
    private bool isGrounded;
    private bool jump;
    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private bool airControle;
    [SerializeField]
    private GameObject[] coin;
    private GameObject enemy;
    AudioSource audio;
    [SerializeField]
    private AudioClip coinSound;
    public Vector3 GetPosition()

    {
        return transform.position;
    }

  
    void Start () {
        facingRight = true;
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        enemy = GameObject.Find("Enemy");
        audio = GetComponent<AudioSource>();
	}
	
	void Update()
    {
        HandleInput();
    }
	void FixedUpdate () {
        float horizontal = Input.GetAxis("Horizontal");

        isGrounded = IsGrounded();
        HandleMovement(horizontal);
        Flip(horizontal);
        HandleAttacks();
        HandleLayers();
        ResetValues();
    }

    private void HandleMovement(float horizontal)
    {
        if(myRigidBody.velocity.y > 0)
        {
            myAnimator.SetBool("land", true);
        }
        if (!myAnimator.GetBool("slide") && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && (isGrounded || airControle))
        {
            myRigidBody.velocity = new Vector2(horizontal * movementSpeed, myRigidBody.velocity.y);  //x = -1 y = 0
        }
        if(isGrounded && jump)
        {
            isGrounded = false;
            myRigidBody.AddForce(new Vector2(0, jumpForce));
            myAnimator.SetTrigger("jump");
        }
        if(slide && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Slide"))
        {
            myAnimator.SetBool("slide", true);
        }
        else if (!this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("Slide"))
        {
            myAnimator.SetBool("slide", false);
        }

        myAnimator.SetFloat("speed", Mathf.Abs (horizontal));
    }

    private void HandleAttacks()
    {
        if (attack && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            myAnimator.SetTrigger("attack");
            myRigidBody.velocity = Vector2.zero;
        }
    }
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            attack = true;
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            slide = true;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            jump = true;
        }
    }

    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

    }
    private void ResetValues()
    {
        attack = false;
        slide = false;
        jump = false;
    }

    private bool IsGrounded()
    {
        if(myRigidBody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoint)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

                for(int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        myAnimator.ResetTrigger("jump");
                        myAnimator.SetBool("land", false);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    private void HandleLayers()
    {
        if (!isGrounded)
        {
            myAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            myAnimator.SetLayerWeight(1, 0);
        }
    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag.Equals("Coin") == true)
        {
            coin[0].SetActive(false);
            //EnemyPatrol tmp = enemy.GetComponent<EnemyPatrol>();
           // tmp.playerHealth++;
            EnemyPatrol.playerHealth++;
            audio.PlayOneShot(coinSound, 0.7F);
        }
        if (target.gameObject.tag.Equals("Coin2") == true)
        {
            coin[1].SetActive(false);
            //EnemyPatrol tmp = enemy.GetComponent<EnemyPatrol>();
            // tmp.playerHealth++;
            EnemyPatrol.playerHealth++;
            audio.PlayOneShot(coinSound, 0.7F);
        }
    }
}

