﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EnemyPatrol : MonoBehaviour {
    [SerializeField]
    private float movementSpeed;
    [SerializeField]
    private float wayPoint1;
    [SerializeField]
    private float wayPoint2;
    [SerializeField]
    private Text health;
    
    public static int playerHealth = 5;
    private Animator enemyAnimator;
    private GameObject restartScreen;


    void Start () {
        enemyAnimator = GetComponent<Animator>();
        restartScreen = GameObject.Find("RestartScreen");
        restartScreen.SetActive(false);
    }
	
	void FixedUpdate()
    {
        // delta = direction * distance
        float delta = wayPoint1 - transform.position.x;
        float distance = Mathf.Abs(delta);
        float direction = delta / distance;
        //1. move towards the waypoint
        transform.position += new Vector3(direction * movementSpeed, 0, 0);
        //2. Check if character has reached waypoint
        if (distance < 1)
        {
            //3. If true swap waypoints
            float tmp = wayPoint1;
            wayPoint1 = wayPoint2;
            wayPoint2 = tmp;
            //4. swap direction
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

    }
	void Update () {
        health.text = "Health: " + playerHealth;
        CheckGameOver();
    }
    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag.Equals("Player") == true)
        {
            playerHealth--;
            enemyAnimator.SetTrigger("attack");
        }
    }
    void CheckGameOver()
    {
        if(playerHealth <= 0)
        {
            restartScreen.SetActive(true);
        }
    }
    public void Easy()
    {
        playerHealth = 7;
    }
    public void Medium()
    {
        playerHealth = 5;
    }
    public void Hard()
    {
        playerHealth = 2;
    }
}
