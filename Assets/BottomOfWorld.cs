﻿using UnityEngine;
using System.Collections;

public class BottomOfWorld : MonoBehaviour {
    private GameObject restartScreen;


    // Use this for initialization
    void Start () {
        restartScreen = GameObject.Find("RestartScreen");
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D target)
    {
        //2. Check if playing is activating it
        if (target.gameObject.tag.Equals("Player") == true)
        {
            restartScreen.SetActive(true);
        }

    }
}
